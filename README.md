# Yet Another Prompt

Just a simple prompt where you can search in the history.

To install it, just run this command :

```bash
sudo curl -sL https://gitlab.com/QuentinN42/yaprompt/-/raw/master/yaprompt -o /usr/bin/yaprompt
sudo chmod +x /usr/bin/yaprompt
```
